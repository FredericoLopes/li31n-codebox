#include <stdio.h>

/* NOTAS:
     - semelhante (mas não igual) ao utilitário wc
     - modifique para usar a função standard isspace
 */

int isspc(int c) {
	return c == ' ' || c == '\n' || c == '\t';
}

int main() {
	int c, pc = '\n';
	int nc = 0, nl = 0, nw = 0;
	while ((c = getchar()) != EOF) {
		nc += 1;
		if (c == '\n') ++nl;
		if (!isspc(c) && isspc(pc)) ++nw;
		pc = c;
	}
	if (pc != '\n') ++nl;
	printf("nl: %d ; nw: %d ; nc: %d\n", nl, nw, nc);
	return 0;
}

