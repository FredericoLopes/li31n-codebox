#include <stdio.h>
#include <ctype.h>

char word[32];
unsigned word_len;

int next_word() {
   int inWord = 0;
   int c, isAlpha;
   word_len = 0;
   for (;;) {
      c = getchar();
      isAlpha = isalpha(c);
      if (!inWord) {
         /* Estado anterior: fora de palavra */
         if (isAlpha) {
            /* Primeiro caracter */
            word[word_len++] = tolower(c);
            inWord = 1;
         } else {
            if (c == EOF) return 0;
         }
      } else {
         /* Estado anterior: dentro de palavra */
         if (isAlpha) {
            /* Mais um caracter */
            word[word_len++] = tolower(c);
         } else {
            word[word_len] = 0;
            return 1;
         }
      }
   }
}

int main() {
   while (next_word()) {
      puts(word);
   }
   return 0;
}

