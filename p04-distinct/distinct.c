#include <stdio.h>
#include <string.h>

/* NOTAS:
     - e se a primeira leitura retornar EOF?
     - crie uma variante que afixa também a contagem de repetições por linha
 */

unsigned readLine(char line[]) {
   unsigned len = 0;
   int c;
   while ((c = getchar()) != '\n' && c != EOF) {
      line[len++] = c;
   }
   line[len] = 0;
   return len;
}

int compareLines(char line1[], char line2[]) {
   unsigned cur = 0;
   while (line1[cur] == line2[cur] && line1[cur]) cur += 1;
   return line1[cur] == line2[cur];
}

#define MAX_LINE 256

char prevLine[MAX_LINE];
char currLine[MAX_LINE];

int main() {
   readLine(prevLine);
   puts(prevLine);
   while (readLine(currLine)) {
      if (!compareLines(currLine, prevLine)) {
         puts(currLine);
         strcpy(prevLine, currLine);
      }
   }
   return 0;
}

